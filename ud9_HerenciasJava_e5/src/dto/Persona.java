package dto;

public abstract class Persona {
	
	//Atributos
	
	private String nombre;
    private char sexo;
    private int edad;
    
    //Constructores
    public Persona() {
    	this.nombre = "";
		this.sexo = 'M';
		this.edad = 0;
    }


	public Persona(String nombre, char sexo, int edad) {
		this.nombre = nombre;
		this.sexo = sexo;
		this.edad = edad;
	}
	
	
	//Getters and Setters
	
	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}


	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	/**
	 * @return the sexo
	 */
	public char getSexo() {
		return sexo;
	}


	/**
	 * @param sexo the sexo to set
	 */
	public void setSexo(char sexo) {
		this.sexo = sexo;
	}


	/**
	 * @return the edad
	 */
	public int getEdad() {
		return edad;
	}


	/**
	 * @param edad the edad to set
	 */
	public void setEdad(int edad) {
		this.edad = edad;
	}

    //obliga a crear el metodo asistencia en clases hijas
	public abstract boolean asistencia(); 
	

}
