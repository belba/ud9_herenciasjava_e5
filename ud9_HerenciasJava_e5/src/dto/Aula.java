package dto;

import java.util.Arrays;
import java.util.Scanner;

public class Aula {
	
	private static final int MAX_ALUMNOS = 10;
	
	// Atributos
	private int idAula;
	private Profesores profesor; // Clase Profesores
	private Estudiantes[] estudiante; //Clase Alumnos
	private String materia;

	//Constructor Aula
	public Aula(int idAula, String materia, Profesores profesor, Estudiantes[] estudiante) {
		this.idAula = idAula;
		//Ciclo que impide que la materia de la aula sea invalida o inexistente.
		boolean isMateria=false;
		do {
			if(materia.equals("matem�ticas")||materia.equals("filosofia")||materia.equals("f�sica")) {
				this.materia = materia;
				isMateria=true;
			} else {
				System.out.println(materia+" no es una materia. Indica una materia: ");
				Scanner scan= new Scanner(System.in);
				materia=scan.next();
		}
		}while(isMateria==false);
		
		this.profesor = profesor;
		this.estudiante = estudiante;		
	}

	//Metodo que comprueva si hay alumnos suficientes para hacer clase
	private boolean hayAlumnos() {
		int mediaClase = MAX_ALUMNOS/2;
		
		if(this.estudiante.length >= mediaClase && this.estudiante.length <= MAX_ALUMNOS) {
			return true;
		}else {
			return false;
		}
	}
	
	//Metodo que comprueva que el professor tenga el mismo metodo que el aula
	private boolean materialDisponible() {
		boolean existeMateria = false;
			
		if(this.profesor.getMateriaAssignada().equals(this.materia)) {
			existeMateria = true;
		}
		return existeMateria;
	}
	
	//Metodo que retorna por String el numero de alumnos aprovados y alumnas aprovadas.
	@Override
	public String toString() {
		String resultado = "";
		
		if(this.profesor.asistencia() == true && materialDisponible() && hayAlumnos()) {
			int aprovadosH = 0;
			int aprovadosM = 0;
			
			for (int i = 0; i < this.estudiante.length; i++) {
				if(this.estudiante[i].getCalificacionActual() >=5 && this.estudiante[i].getSexo() == 'H' && this.estudiante[i].asistencia()) {
					aprovadosH++;
				}
				if(this.estudiante[i].getCalificacionActual() >=5 && this.estudiante[i].getSexo() == 'M' && this.estudiante[i].asistencia()) {
					aprovadosM++;
				}
				resultado = "Alumnos aprovados: "+ aprovadosH + "\nAlumnas aprovadas: "+ aprovadosM;
			}
			
			
		}else {
			resultado = "No se puede dar clase";
		}
		
		return resultado;
	}
	
	/**
	 * @return the idAula
	 */
	public int getIdAula() {
		return idAula;
	}


	/**
	 * @param idAula the idAula to set
	 */
	public void setIdAula(int idAula) {
		this.idAula = idAula;
	}


	/**
	 * @return the profesor
	 */
	public Profesores getProfesor() {
		return profesor;
	}


	/**
	 * @param profesor the profesor to set
	 */
	public void setProfesor(Profesores profesor) {
		this.profesor = profesor;
	}


	/**
	 * @return the estudiante
	 */
	public Estudiantes[] getEstudiante() {
		return estudiante;
	}


	/**
	 * @param estudiante the estudiante to set
	 */
	public void setEstudiante(Estudiantes[] estudiante) {
		this.estudiante = estudiante;
	}


	/**
	 * @return the materia
	 */
	public String getMateria() {
		return materia;
	}


	/**
	 * @param materia the materia to set
	 */
	public void setMateria(String materia) {
		this.materia = materia;
	}

	
	
}
