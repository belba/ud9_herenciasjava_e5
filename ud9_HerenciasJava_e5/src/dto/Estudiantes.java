package dto;

public class Estudiantes extends Persona {

	//Atributos
	private int calificacionActual;
	private int novillos; 

	//constructor vacio con campos aleatorios
	public Estudiantes() {
		super();
		this.calificacionActual = (int) Math.floor(Math.random()*10+1);
		this.novillos =  (int) Math.floor(Math.random()*100+1);
		
	}
	
	//constructor con campos
	public Estudiantes(String nombre, char sexo, int edad, int calificacionActual, int novillos) {
		super(nombre, sexo, edad);
		
		this.calificacionActual = calificacionActual;		
		this.novillos = novillos;
	}
	
	//METODOS -----------------------------
//comprueva que el alumno tiene asistencia
	public boolean asistencia() {
		// TODO Auto-generated method stub
		
		if(this.novillos > 50) {
			return true;
		}else {
			return false;
		}
	}
	
	//Getters and Settings
	/**
	 * @return the calificacionActual
	 */
	public int getCalificacionActual() {
		return calificacionActual;
	}

	/**
	 * @param calificacionActual the calificacionActual to set
	 */
	public void setCalificacionActual(int calificacionActual) {
		this.calificacionActual = calificacionActual;
	}

	/**
	 * @return the novillos
	 */
	public int getNovillos() {
		return novillos;
	}

	/**
	 * @param novillos the novillos to set
	 */
	public void setNovillos(int novillos) {
		this.novillos = novillos;
	}

	
	
	
}
