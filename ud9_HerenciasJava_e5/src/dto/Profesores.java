package dto;

import java.util.Scanner;

public class Profesores extends Persona {
	
	//Atributos
	private String materiaAssignada;
	private int disponibilidad;

	private static final String[] MATERIAL_DISPONIBLE =  {"matem�ticas","filosof�a","f�sica"};
	
	//Constructor
	
	public Profesores() {		
		super();
		// Asignamos por defecto una materia aleatoria
		this.materiaAssignada = MATERIAL_DISPONIBLE[(int) Math.floor(Math.random()*3+1)];
		this.disponibilidad =  (int) Math.floor(Math.random()*100+1);
		
	}
	
	public Profesores(String nombre, char sexo, int edad, String materiaAssignada, int disponibilidad) {
		super(nombre, sexo, edad);
		boolean isMateria=false;
		do {
			if(materiaAssignada.equals("matem�ticas")||materiaAssignada.equals("filosof�a")||materiaAssignada.equals("f�sica")) {
				this.materiaAssignada = materiaAssignada;
				isMateria=true;
			} else {
				System.out.println(materiaAssignada+" no es una materia. Indica una materia: ");
				Scanner scan= new Scanner(System.in);
				materiaAssignada=scan.next();
			} 
		}while(isMateria==false);
		
		this.disponibilidad = disponibilidad;
	}

	//Metodos

	public boolean asistencia() {
		// TODO Auto-generated method stub
		
		if(this.disponibilidad > 20) { // metodo que devuelve un true o false si la disponibilidad del profesor es menos que 20% false
			return true;
		}else {
			return false;
		}
	}

	//Getters and Settings

	/**
	 * @return the disponibilidad
	 */
	public int getDisponibilidad() {
		return disponibilidad;
	}

	/**
	 * @return the materiaAssignada
	 */
	public String getMateriaAssignada() {
		return materiaAssignada;
	}

	/**
	 * @param materiaAssignada the materiaAssignada to set
	 */
	public void setMateriaAssignada(String materiaAssignada) {
		this.materiaAssignada = materiaAssignada;
	}

	/**
	 * @param disponibilidad the disponibilidad to set
	 */
	public void setDisponibilidad(int disponibilidad) {
		this.disponibilidad = disponibilidad;
	}

	
	
	

}
