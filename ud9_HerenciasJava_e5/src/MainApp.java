import dto.Aula;
import dto.Estudiantes;
import dto.Profesores;

public class MainApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//introducimos los estudiantes y ya les asignamos los valores por defecto
        Estudiantes e1 = new Estudiantes("Pepe", 'H', 14, 7, 60);
        Estudiantes e2 = new Estudiantes("Maria", 'M', 14, 8, 80);
        Estudiantes e3 = new Estudiantes("Juan", 'H', 14, 4, 80);
        Estudiantes e4 = new Estudiantes("Juana", 'M', 14, 9, 80);
        Estudiantes e5 = new Estudiantes("Mariam", 'M', 14, 4, 50);
        Estudiantes e6 = new Estudiantes("Pepa", 'M', 14, 5, 40);

        //los guardamos en un array
        Estudiantes[] estudiantes = {e1,e2,e3,e4,e5,e6};

        //Asignamos valores al profesor
        Profesores p1 = new Profesores("Santiago", 'H', 42, "matemáticas", 30);
        Profesores p2 = new Profesores("Santiago", 'H', 42, "filosofía", 10);

// creamos las aulas
        Aula a1 = new Aula(1,"matemáticas", p1, estudiantes);
        Aula a2 = new Aula(2,"matemáticas", p2, estudiantes);
        
        //VEamos el resultado de toString
        System.out.println("Aula 1:");
        System.out.println(a1);
        System.out.println("\nAula 2:");
        System.out.println(a2);
	}

}
